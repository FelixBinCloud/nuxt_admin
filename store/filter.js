const state = ({
  isShowBread: false,
  current_path: {},
  isEdit:false,
  isAdd:false
});
const getters = {
  isEdit:(state)=>{
    return state.isEdit;
  },
  isAdd:(state)=>{
    return state.isAdd;
  }
};

const mutations = {
  //面包屑导航是否显示
  SetIsShowBread(state, value) {
    state.isShowBread = value;
  },

  //面包屑导航
  SetCurrentPath(state, value) {
    state.current_path = value;
  },

  //是否编辑
  SetIsEdit(state, value) {
    state.isEdit = value;
    console.log("isEdit",value)
  },
  //是否添加
  SetIsAdd(state, value) {
    state.isAdd = value;
    console.log("isAdd",value)
  },
};

const actions = {
  // async getInfo({state, commit}, val) {
  //   commit('SET_IsShowBread', val);
  // }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
